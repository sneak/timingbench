default: test

test:
	go test -v -count=1 ./...

fmt:
	go fmt ./...

lint:
	golangci-lint run
	sh -c 'test -z "$$(gofmt -l .)"'
