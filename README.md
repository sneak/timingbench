# TimingBench

`TimingBench` is a Go module for benchmarking the execution time of a
function. It runs the function a specified number of times and returns the
minimum, maximum, mean, and median execution times, along with other
relevant statistics. It also supports context for cancellation.

## Features

-   Measure min, max, mean, and median execution times of a function.
-   Support for context-based cancellation.
-   Provides detailed timing statistics.

## Installation

To install the `timingbench` package, use the following command:

```sh
go get git.eeqj.de/sneak/timingbench
```

## Usage

Here's a simple example of how to use the `timingbench` module:

```go
package main

import (
    "context"
    "fmt"
    "math/rand"
    "time"
    "git.eeqj.de/sneak/timingbench"
)

// Example function to benchmark
func sampleFunction() error {
    time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
    return nil
}

func main() {
    // Seed the random number generator.
    rand.Seed(time.Now().UnixNano())

    // Define the number of iterations.
    iterations := 10

    // Create a context with a timeout for cancellation.
    ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
    defer cancel()

    // Measure the execution times of the sample function.
    result, err := timingbench.TimeFunction(ctx, sampleFunction, iterations)
    if err != nil {
        fmt.Printf("Error measuring function: %v\n", err)
        return
    }

    // Print the timing results.
    fmt.Println(result.String())
}
```

## TimingResult

The `TimingResult` struct holds the timing statistics for the function executions:

```go
type TimingResult struct {
    Min         time.Duration // Minimum execution time
    Max         time.Duration // Maximum execution time
    Mean        time.Duration // Mean execution time
    Median      time.Duration // Median execution time
    StartTime   time.Time // Start time of the benchmarking
    EndTime     time.Time // End time of the benchmarking
    Duration    time.Duration // Total duration of the benchmarking
    Iterations  int // Number of iterations
}
```

## License

This project is licensed under the WTFPL License. See the
[LICENSE](./LICENSE) file for details.

## Contributing

Contributions are welcome! Please send patches via email.

## Author

`timingbench` was written by [sneak](mailto:sneak@sneak.berlin) ([website](https://sneak.berlin)).
