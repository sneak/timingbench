module sneak.berlin/go/timingbench

go 1.22.2

require (
	github.com/schollz/progressbar/v3 v3.14.2
	gonum.org/v1/gonum v0.15.0
)

require (
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/term v0.20.0 // indirect
)
